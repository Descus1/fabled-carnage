﻿namespace Enums
{
    public enum EffectTypes
    {
        None,
        Speedup, 
        Slow,
        Shield,
        Evade
    }
}