﻿using UnityEngine;

namespace Utility
{
    [System.Serializable]
    public class ColorPrefab
    {
        public Color color;
        public GameObject prefab;
    }
}
