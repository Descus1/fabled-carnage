﻿namespace Interfaces
{
    public interface ISKillable
    {
        void Kill();
    }
}